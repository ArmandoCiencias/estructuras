package mx.unam.fciencias.estructuras.lineales;

public interface Cola<E> {
    void insertar(E elemento);
    E remover();
}
